package com.dmanluc.orangebanktest.app.di.module

import com.dmanluc.orangebanktest.data.api.OrangeBankApi
import com.dmanluc.orangebanktest.data.contract.TransactionOutputResponse
import com.dmanluc.orangebanktest.data.repository.TransactionsRepositoryImpl
import com.dmanluc.orangebanktest.data.transformer.Transformer
import com.dmanluc.orangebanktest.domain.entity.Transaction
import com.dmanluc.orangebanktest.domain.repository.TransactionsRepository
import dagger.Module
import dagger.Provides

/**
 * Module class for providing repository dependencies to their consumers given in AppComponent class
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
@Module
class RepositoryModule {

    @Provides
    fun provideTransactionsRepository(
        api: OrangeBankApi,
        transformer: @JvmSuppressWildcards Transformer<List<TransactionOutputResponse>, List<Transaction>>): TransactionsRepository {
        return TransactionsRepositoryImpl(api, transformer)
    }

}