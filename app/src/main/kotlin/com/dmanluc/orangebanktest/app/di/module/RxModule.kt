package com.dmanluc.orangebanktest.app.di.module

import com.dmanluc.orangebanktest.app.utils.AppRxSchedulersProvider
import com.dmanluc.orangebanktest.app.utils.RxSchedulersProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Module class for providing reactive schedulers dependencies to their consumers given in AppComponent class
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
@Module
class RxModule {

    @Provides
    @Singleton
    fun provideSchedulers(): RxSchedulersProvider = AppRxSchedulersProvider()

}