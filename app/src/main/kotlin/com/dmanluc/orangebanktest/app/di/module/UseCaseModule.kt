package com.dmanluc.orangebanktest.app.di.module

import com.dmanluc.orangebanktest.app.utils.RxSchedulersProvider
import com.dmanluc.orangebanktest.domain.entity.Transaction
import com.dmanluc.orangebanktest.domain.repository.TransactionsRepository
import com.dmanluc.orangebanktest.domain.usecase.TransactionsUseCase
import com.dmanluc.orangebanktest.domain.usecase.base.BaseUseCase
import dagger.Module
import dagger.Provides

/**
 * Module class for providing use case dependencies to their consumers given in AppComponent class
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
@Module
class UseCaseModule {

    @Provides
    fun provideTransactionsUseCase(
        repository: TransactionsRepository,
        schedulersProvider: RxSchedulersProvider): @JvmSuppressWildcards BaseUseCase<List<Transaction>, Unit> {
        return TransactionsUseCase(repository, schedulersProvider)
    }

}