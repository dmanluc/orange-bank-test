package com.dmanluc.orangebanktest.app.di.module

import com.dmanluc.orangebanktest.data.api.OrangeBankApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Module class for providing API dependencies to their consumers given in AppComponent class
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideOrangeBankApi(retrofit: Retrofit): OrangeBankApi =
        retrofit.create<OrangeBankApi>(OrangeBankApi::class.java)

}