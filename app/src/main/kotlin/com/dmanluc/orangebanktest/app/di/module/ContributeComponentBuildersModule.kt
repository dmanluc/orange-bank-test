package com.dmanluc.orangebanktest.app.di.module

import com.dmanluc.orangebanktest.app.di.scope.PerActivity
import com.dmanluc.orangebanktest.presentation.ui.transactions.TransactionsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Specific Dagger module class to handle the creation of subcomponents which perform members-injection for a concrete subtype of a core Android type
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
@Module
abstract class ContributeComponentBuildersModule {

    @PerActivity
    @ContributesAndroidInjector
    abstract fun bindTransactionsActivity(): TransactionsActivity

}