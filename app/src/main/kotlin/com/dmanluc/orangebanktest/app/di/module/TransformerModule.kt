package com.dmanluc.orangebanktest.app.di.module

import com.dmanluc.orangebanktest.data.contract.TransactionOutputResponse
import com.dmanluc.orangebanktest.data.transformer.TransactionsTransformer
import com.dmanluc.orangebanktest.data.transformer.Transformer
import com.dmanluc.orangebanktest.domain.entity.Transaction
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Module class for providing repository transformer dependencies to their consumers given in AppComponent class

 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
@Module
class TransformerModule {

    @Provides
    @Singleton
    fun provideTransactionsTransformer(): @JvmSuppressWildcards Transformer<List<TransactionOutputResponse>, List<Transaction>> {
        return TransactionsTransformer()
    }

}