package com.dmanluc.orangebanktest.app.utils

import io.reactivex.Scheduler

/**
 * Interface defining reactive schedulers provider
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
interface RxSchedulersProvider {

    fun ui(): Scheduler

    fun io(): Scheduler

}