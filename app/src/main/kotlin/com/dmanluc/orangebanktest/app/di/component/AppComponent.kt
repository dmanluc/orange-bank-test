package com.dmanluc.orangebanktest.app.di.component

import com.dmanluc.orangebanktest.app.App
import com.dmanluc.orangebanktest.app.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * App component interface for dependency injection (DI)
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ContributeComponentBuildersModule::class,
        UseCaseModule::class,
        NetworkModule::class,
        RxModule::class,
        RepositoryModule::class,
        ApiModule::class,
        TransformerModule::class]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent

    }

}