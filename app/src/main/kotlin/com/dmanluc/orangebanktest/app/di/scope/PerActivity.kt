package com.dmanluc.orangebanktest.app.di.scope

import javax.inject.Scope

/**
 * Custom activity scope for DI
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity