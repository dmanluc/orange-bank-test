package com.dmanluc.orangebanktest.app.di.module

import com.dmanluc.orangebanktest.app.App
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Module class for providing dependencies to their consumers given in AppComponent class
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
@Module
class AppModule {

    @Provides
    @Singleton
    @Named("applicationComponent")
    fun provideApplication(application: App): App = application

}