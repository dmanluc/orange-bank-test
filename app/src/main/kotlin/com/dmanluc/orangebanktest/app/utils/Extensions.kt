package com.dmanluc.orangebanktest.app.utils

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.text.style.SuperscriptSpan
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import kotlin.math.abs
import kotlin.math.round
import kotlin.math.sign

/**
 * App extension utils
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */

fun View.show() {
    visibility = VISIBLE
}

fun View.hide() {
    visibility = GONE
}

fun Double.isPositive(): Boolean {
    return sign >= 0
}

fun Double.isNegative(): Boolean {
    return sign < 0
}

fun TextView.prettyTransactionAmount(amount: Double, currency: String) {
    val integerPart = amount.toInt().toString()
    val decimalPart = round(abs(amount.rem(1.00) * 100)).toString().take(2)

    text = SpannableStringBuilder("$integerPart$decimalPart $currency").apply {
        setSpan(
            SuperscriptSpan(), integerPart.length, this.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        setSpan(
            RelativeSizeSpan(0.65f), integerPart.length, this.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
}