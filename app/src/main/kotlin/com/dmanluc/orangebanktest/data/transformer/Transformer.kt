package com.dmanluc.orangebanktest.data.transformer

/**
 * Transformer interface to adapt an specific output contract from JSON response to the specific domain entity of the app
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018.
 */
interface Transformer<in R, out T> {

    fun transformContractToModel(outputContract: R): T

}
