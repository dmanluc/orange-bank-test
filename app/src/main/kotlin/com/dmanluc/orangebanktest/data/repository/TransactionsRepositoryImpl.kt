package com.dmanluc.orangebanktest.data.repository

import com.dmanluc.orangebanktest.data.api.OrangeBankApi
import com.dmanluc.orangebanktest.data.contract.TransactionOutputResponse
import com.dmanluc.orangebanktest.data.transformer.Transformer
import com.dmanluc.orangebanktest.domain.entity.Transaction
import com.dmanluc.orangebanktest.domain.repository.TransactionsRepository
import io.reactivex.Single

/**
 * Implementation of repository pattern to retrieve transactions data from API data source
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
class TransactionsRepositoryImpl
constructor(
    private val api: OrangeBankApi,
    private val transformer: Transformer<List<TransactionOutputResponse>, List<Transaction>>
) : TransactionsRepository {

    override fun getTransactions(): Single<List<Transaction>> {
        return api.getTransactions().map { transformer.transformContractToModel(it) }
    }

}