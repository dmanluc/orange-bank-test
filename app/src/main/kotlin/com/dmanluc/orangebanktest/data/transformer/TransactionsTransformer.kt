package com.dmanluc.orangebanktest.data.transformer

import com.dmanluc.orangebanktest.data.contract.TransactionOutputResponse
import com.dmanluc.orangebanktest.domain.entity.Transaction
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeParseException

/**
 * Transformer implementation to map transactions API response model to the specific app domain entity according to business logic.
In particular, only well-formed ISO DATE transactions dates are processed and if its description is null or empty, "(Empty Description)" is added to it.
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
class TransactionsTransformer : Transformer<List<TransactionOutputResponse>, List<Transaction>> {

    override fun transformContractToModel(outputContract: List<TransactionOutputResponse>): List<Transaction> {
        return outputContract.map {

            val isoInstantDate = try {
                LocalDateTime.from(DateTimeFormatter.ISO_DATE_TIME.parse(it.date))
            } catch (parseException: DateTimeParseException) {
                null
            }

            Transaction(
                it.id,
                isoInstantDate,
                it.amount,
                it.fee ?: 0.0,
                if (it.description.isNullOrBlank()) "(Empty Description)" else it.description
            )
        }.filter { it.date != null }
    }

}