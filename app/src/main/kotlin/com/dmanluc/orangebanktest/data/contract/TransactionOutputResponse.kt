package com.dmanluc.orangebanktest.data.contract

import com.google.gson.annotations.SerializedName

/**
 * API response model for transactions response
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
class TransactionOutputResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("date") val date: String,
    @SerializedName("amount") val amount: Double,
    @SerializedName("fee") val fee: Double?,
    @SerializedName("description") val description: String?
)