package com.dmanluc.orangebanktest.data.api

import com.dmanluc.orangebanktest.data.contract.TransactionOutputResponse
import io.reactivex.Single
import retrofit2.http.GET

/**
 * API definition from which transactions will be fetched
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
interface OrangeBankApi {

    @GET("bins/1a30k8")
    fun getTransactions(): Single<List<TransactionOutputResponse>>

}