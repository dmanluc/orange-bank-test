package com.dmanluc.orangebanktest.domain.repository

import com.dmanluc.orangebanktest.domain.entity.Transaction
import io.reactivex.Single

/**
 * Interface for transactions repository pattern to be used with reactive programming
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
interface TransactionsRepository {

    fun getTransactions(): Single<List<Transaction>>

}