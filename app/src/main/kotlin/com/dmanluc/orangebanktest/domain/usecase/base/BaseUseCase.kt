package com.dmanluc.orangebanktest.domain.usecase.base

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Base abstract use case/interactor to be used in the context of reactive programming
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018.
 */
abstract class BaseUseCase<T, in Params> {

    private val disposables: CompositeDisposable = CompositeDisposable()

    /**
     * Builds an [Observable] which will be used when executing the current [BaseUseCase].
     */
    abstract fun buildUseCaseObservable(params: Params? = null): Single<T>

    abstract fun provideBackgroundScheduler(): Scheduler

    abstract fun provideUiScheduler(): Scheduler

    /**
     * Executes the current use case.
     *
     * @param observer [DisposableSingleObserver] which will be listening to the observable build
     * by [.buildUseCaseObservable] ()} method.
     * @param params Parameters (Optional) used to build/execute this use case.
     */
    fun execute(observer: DisposableSingleObserver<T>, params: Params? = null) {

        val observable = this.buildUseCaseObservable(params)
            .subscribeOn(provideBackgroundScheduler())
            .observeOn(provideUiScheduler())

        disposables += observable.subscribeWith(observer)
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.clear()
        }
    }

    operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
        add(disposable)
    }

}