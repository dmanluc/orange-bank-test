package com.dmanluc.orangebanktest.domain.entity

import org.threeten.bp.LocalDateTime
import java.io.Serializable

/**
 * Domain model entity for transaction item
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
class Transaction(
    val id: Int,
    val date: LocalDateTime?,
    val amount: Double,
    val fee: Double = 0.0,
    val description: String = ""
) : Serializable