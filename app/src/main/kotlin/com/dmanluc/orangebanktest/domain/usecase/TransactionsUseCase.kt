package com.dmanluc.orangebanktest.domain.usecase

import com.dmanluc.orangebanktest.app.utils.RxSchedulersProvider
import com.dmanluc.orangebanktest.domain.entity.Transaction
import com.dmanluc.orangebanktest.domain.repository.TransactionsRepository
import com.dmanluc.orangebanktest.domain.usecase.base.BaseUseCase
import io.reactivex.Scheduler
import io.reactivex.Single

/**
 * Uae case that allows to fetch transactions data
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
class TransactionsUseCase
constructor(private val repository: TransactionsRepository, private val schedulersProvider: RxSchedulersProvider) : BaseUseCase<List<Transaction>, Unit>() {

    override fun buildUseCaseObservable(params: Unit?): Single<List<Transaction>> {
        return repository.getTransactions()
    }

    override fun provideBackgroundScheduler(): Scheduler {
        return schedulersProvider.io()
    }

    override fun provideUiScheduler(): Scheduler {
        return schedulersProvider.ui()
    }

}