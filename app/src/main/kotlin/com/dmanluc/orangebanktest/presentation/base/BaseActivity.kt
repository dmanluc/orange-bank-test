package com.dmanluc.orangebanktest.presentation.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.Nullable
import android.support.v7.app.ActionBar
import android.view.MenuItem
import dagger.android.support.DaggerAppCompatActivity

/**
 * Base activity abstract template for MVP presentation pattern with DI provided by Dagger framework
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
@Suppress("UNCHECKED_CAST")
abstract class BaseActivity<in V : BaseView, out P : Presenter<V>> : DaggerAppCompatActivity() {

    @get:LayoutRes
    protected abstract val layoutId: Int

    protected abstract val screenTitle: String?

    protected abstract val presenter: P

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        setActionBar(screenTitle, showBackArrow())
        presenter.attachView(this as V)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    protected abstract fun showBackArrow(): Boolean

    open fun setActionBar(heading: String?, enableBackArrow: Boolean) {
        supportActionBar?.apply {
            displayOptions = ActionBar.DISPLAY_SHOW_TITLE
            title = heading.orEmpty()
            setDisplayHomeAsUpEnabled(enableBackArrow)
            setDisplayShowHomeEnabled(enableBackArrow)
            elevation = 0f
            show()
        }
    }
}