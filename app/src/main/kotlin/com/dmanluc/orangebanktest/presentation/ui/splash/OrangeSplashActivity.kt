package com.dmanluc.orangebanktest.presentation.ui.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dmanluc.orangebanktest.presentation.ui.transactions.TransactionsActivity

/**
 * Initial splash screen with Orange Bank logo
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    06/11/2018.
 */
class OrangeSplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity(Intent(this, TransactionsActivity::class.java))
        finish()
    }

}