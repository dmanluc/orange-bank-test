package com.dmanluc.orangebanktest.presentation.base

/**
 * Base presenter interface template for MVP pattern
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
interface Presenter<in V : BaseView> {

    /**
     * Attach presenter's view.
     */
    fun attachView(view: V)

    /**
     * Detach presenter's view
     */
    fun detachView()

    /**
     * Check if presenter has attached its view
     */
    fun isViewAttached(): Boolean

}
