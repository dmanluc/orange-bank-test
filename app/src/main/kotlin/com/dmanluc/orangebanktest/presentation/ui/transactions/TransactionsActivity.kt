package com.dmanluc.orangebanktest.presentation.ui.transactions

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.animation.AnimationUtils
import com.andrognito.flashbar.Flashbar
import com.dmanluc.orangebanktest.R
import com.dmanluc.orangebanktest.app.utils.hide
import com.dmanluc.orangebanktest.app.utils.isPositive
import com.dmanluc.orangebanktest.app.utils.prettyTransactionAmount
import com.dmanluc.orangebanktest.app.utils.show
import com.dmanluc.orangebanktest.domain.entity.Transaction
import com.dmanluc.orangebanktest.presentation.base.BaseActivity
import com.dmanluc.orangebanktest.presentation.ui.transactions.adapter.TransactionsAdapter
import kotlinx.android.synthetic.main.activity_transactions.*
import kotlinx.android.synthetic.main.item_transaction_header.*
import kotlinx.android.synthetic.main.item_transactions_data_not_available.*
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import org.threeten.bp.format.TextStyle
import java.io.Serializable
import java.util.*
import javax.inject.Inject

/**
 * Main activity which shows transactions data in a list on the screen by means of a recycler view and the most recent one highlighted at the top of the screen.
The way of showing transactions is handled by the presenter of this activity and they are ordered by most recent date and a different color indicates its positive or negative amount
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
class TransactionsActivity : BaseActivity<TransactionsView, TransactionsPresenter>(), TransactionsView {

    companion object {

        private const val KEY_TRANSACTIONS = "key:serializable:transactions"

    }

    @Inject
    lateinit var internalPresenter: TransactionsPresenter

    private val transactionsAdapter = TransactionsAdapter()

    override val layoutId: Int
        get() = R.layout.activity_transactions
    override val screenTitle: String?
        get() = getString(R.string.app_name)
    override val presenter: TransactionsPresenter
        get() = internalPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupUi()

        savedInstanceState?.let {
            hideLoading()
            (savedInstanceState.getSerializable(KEY_TRANSACTIONS) as List<Transaction>).apply {
                presenter.loadRestoredTransactions(this)
            }
        } ?: run {
            presenter.loadTransactions(firstLoad = transactionsAdapter.itemCount == 0)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putSerializable(KEY_TRANSACTIONS, presenter.getCurrentTransactions() as Serializable)
        super.onSaveInstanceState(outState)
    }

    override fun showBackArrow(): Boolean = false

    override fun showLoading() {
        loading.show()
    }

    override fun hideLoading() {
        loading.hide()
        swipeLayout.isRefreshing = false
    }

    @SuppressLint("SetTextI18n")
    override fun showHeaderTransaction(transaction: Transaction) {
        val amount = transaction.amount + transaction.fee

        headerTransactionDay.text = transaction.date?.dayOfMonth.toString()
        headerTransactionMonth.text = "${transaction.date?.month?.getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault())}."
        headerTransactionTime.text = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(transaction.date)
        headerTransactionLabel.show()
        headerTransactionDescription.text = transaction.description
        headerTransactionAmount.apply {
            prettyTransactionAmount(amount, "€")
        }

        if (amount.isPositive()) applyPositiveHeaderAmountStyle() else applyNegativeHeaderAmountStyle()
    }

    override fun showTransactions(transactionList: List<Transaction>) {
        val animationController =
            AnimationUtils.loadLayoutAnimation(applicationContext, R.anim.layout_animation_from_bottom)
        rvTransactions.layoutAnimation = animationController
        transactionsAdapter.updateItems(transactionList)
        rvTransactions.scheduleLayoutAnimation()
    }

    override fun showErrorMessage(message: String) {
        Flashbar.Builder(this)
            .gravity(Flashbar.Gravity.BOTTOM)
            .title(R.string.transactions_activity_network_error_title)
            .message(message)
            .duration(5 * 1000)
            .backgroundColorRes(R.color.colorPrimaryDark)
            .listenBarTaps(object : Flashbar.OnTapListener {
                override fun onTap(flashbar: Flashbar) {
                    flashbar.dismiss()
                }
            })
            .enableSwipeToDismiss()
            .build()
            .show()
    }

    override fun showTransactionsNotAvailableScreen() {
        transactionsNotAvailableView.show()
    }

    override fun hideTransactionsNotAvailableScreen() {
        transactionsNotAvailableView.hide()
    }

    private fun setupUi() {
        rvTransactions.apply {
            layoutManager = LinearLayoutManager(this@TransactionsActivity)
            adapter = transactionsAdapter
        }

        swipeLayout.apply {
            setOnRefreshListener {
                presenter.loadTransactions(
                    firstLoad = transactionsAdapter.itemCount == 0,
                    showLoading = false
                )
            }
            setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
            )
        }
    }

    private fun applyPositiveHeaderAmountStyle() {
        headerTransactionAmount.setTextColor(ContextCompat.getColor(this, R.color.positiveAmountTransaction))
        headerTransactionIndicator.setBackgroundColor(ContextCompat.getColor(this, R.color.positiveAmountTransaction))
    }

    private fun applyNegativeHeaderAmountStyle() {
        headerTransactionAmount.setTextColor(ContextCompat.getColor(this, R.color.negativeAmountTransaction))
        headerTransactionIndicator.setBackgroundColor(ContextCompat.getColor(this, R.color.negativeAmountTransaction))
    }

}
