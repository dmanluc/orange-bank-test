package com.dmanluc.orangebanktest.presentation.base

import java.lang.ref.WeakReference

/**
 * Base presenter abstract template for MVP pattern
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
abstract class BasePresenter<V : BaseView> : Presenter<V> {

    val view: V?
        get() = weakReference?.get()

    private var weakReference: WeakReference<V>? = null

    override fun attachView(view: V) {
        if (!isViewAttached()) {
            weakReference = WeakReference(view)
        }
    }

    override fun detachView() {
        weakReference?.clear()
        weakReference = null
    }

    override fun isViewAttached(): Boolean = weakReference?.get() != null

}
