package com.dmanluc.orangebanktest.presentation.ui.transactions

import com.dmanluc.orangebanktest.domain.entity.Transaction
import com.dmanluc.orangebanktest.presentation.base.BaseView

/**
 * MVP view interface for {@link TransactionsActivity}
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
interface TransactionsView : BaseView {

    fun showHeaderTransaction(transaction: Transaction)

    fun showTransactions(transactionList: List<Transaction>)

    fun showErrorMessage(message: String)

    fun showTransactionsNotAvailableScreen()

    fun hideTransactionsNotAvailableScreen()

}