package com.dmanluc.orangebanktest.presentation.base

/**
 * Base view interface template for MVP pattern
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018.
 */
interface BaseView {

    fun showLoading()

    fun hideLoading()

}