package com.dmanluc.orangebanktest.presentation.ui.transactions

import com.dmanluc.orangebanktest.domain.entity.Transaction
import com.dmanluc.orangebanktest.domain.usecase.base.BaseUseCase
import com.dmanluc.orangebanktest.presentation.base.BasePresenter
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * MVP presenter for {@link TransactionsActivity} which handles its presentation UI logic
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    17/11/2018
 */
class TransactionsPresenter
@Inject constructor(private val transactionsUseCase: @JvmSuppressWildcards BaseUseCase<List<Transaction>, Unit>) :
    BasePresenter<TransactionsView>() {

    private val sortedTransactions = mutableListOf<Transaction>()

    fun loadTransactions(firstLoad: Boolean, showLoading: Boolean = false) {
        if (showLoading) view?.showLoading()
        transactionsUseCase.execute(object : DisposableSingleObserver<List<Transaction>>() {
            override fun onSuccess(t: List<Transaction>) {
                view?.hideLoading()

                if (t.isEmpty() && firstLoad) {
                    view?.showTransactionsNotAvailableScreen()
                } else {
                    view?.hideTransactionsNotAvailableScreen()

                    sortedTransactions.apply {
                        clear()
                        addAll(t.sortedByDescending { it.date }.groupBy { it.id }.map { it.value[0] })
                    }

                    view?.showHeaderTransaction(sortedTransactions.first())
                    view?.showTransactions(sortedTransactions.let { it.takeLast(it.size - 1) })
                }
            }

            override fun onError(e: Throwable) {
                view?.hideLoading()
                view?.showErrorMessage(e.message.orEmpty())

                if (firstLoad) view?.showTransactionsNotAvailableScreen()
            }
        })
    }

    fun getCurrentTransactions() = sortedTransactions

    fun loadRestoredTransactions(transactions: List<Transaction>) = sortedTransactions.apply {
        if (transactions.isNotEmpty()) {
            view?.showHeaderTransaction(transactions.first())
            view?.showTransactions(transactions.takeLast(transactions.size - 1))
        }
        clear()
        addAll(transactions)
    }

    override fun detachView() {
        transactionsUseCase.dispose()
        super.detachView()
    }

}