package com.dmanluc.orangebanktest.presentation.ui.transactions.adapter

import android.annotation.SuppressLint
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dmanluc.orangebanktest.R
import com.dmanluc.orangebanktest.app.utils.isPositive
import com.dmanluc.orangebanktest.app.utils.prettyTransactionAmount
import com.dmanluc.orangebanktest.domain.entity.Transaction
import kotlinx.android.synthetic.main.item_transaction.view.*
import org.threeten.bp.format.TextStyle
import java.util.*

/**
 * Component which handles creation of transaction view holders and its binding to be shown in the recycler view of {@link TransactionsActivity}.
Transactions are shown in a common list way but this could be improved by adding sticky date headers, grouping transactions on the same date.
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */
class TransactionsAdapter : RecyclerView.Adapter<TransactionsAdapter.TransactionHolder>() {

    private val items: MutableList<Transaction> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionHolder {
        return TransactionHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_transaction, parent, false))
    }

    override fun onBindViewHolder(holder: TransactionHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun updateItems(products: List<Transaction>) {
        items.apply {
            clear()
            addAll(products)
        }
        notifyDataSetChanged()
    }

    /**
     * View holder that shows transaction data. To be precise, the logic of its binding should be handled by the presenter associated to the activity that hosts this adapter.
     * But for this scenario, in which the UI binding logic is quite simple, it is acceptable to handle it inside the bind method
     *
     */
    class TransactionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bind(transaction: Transaction) {
            itemView.apply {
                val amount = transaction.amount + transaction.fee

                transactionDay.text = transaction.date?.dayOfMonth.toString()
                transactionMonth.text = "${transaction.date?.month?.getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault())}."
                transactionDescription.text = transaction.description
                transactionAmount.apply {
                    prettyTransactionAmount(amount, "€")
                }

                if (amount.isPositive()) applyPositiveAmountStyle() else applyNegativeAmountStyle()
            }
        }

        private fun applyPositiveAmountStyle() {
            itemView.apply {
                transactionAmount.setTextColor(ContextCompat.getColor(context, R.color.positiveAmountTransaction))
                transactionIndicator.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.positiveAmountTransaction
                    )
                )
            }
        }

        private fun applyNegativeAmountStyle() {
            itemView.apply {
                transactionAmount.setTextColor(ContextCompat.getColor(context, R.color.negativeAmountTransaction))
                transactionIndicator.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.negativeAmountTransaction
                    )
                )
            }
        }

    }

}