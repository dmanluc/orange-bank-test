package com.dmanluc.orangebanktest.presentation.ui.transactions

import com.dmanluc.orangebanktest.app.utils.RxSchedulersProvider
import com.dmanluc.orangebanktest.data.api.OrangeBankApi
import com.dmanluc.orangebanktest.data.contract.TransactionOutputResponse
import com.dmanluc.orangebanktest.data.repository.TransactionsRepositoryImpl
import com.dmanluc.orangebanktest.data.transformer.TransactionsTransformer
import com.dmanluc.orangebanktest.domain.entity.Transaction
import com.dmanluc.orangebanktest.domain.usecase.TransactionsUseCase
import com.dmanluc.orangebanktest.utils.buildTransactionList
import com.dmanluc.orangebanktest.utils.buildTransactionListResponse
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

/**
 * Unit test of {@link TransactionsPresenter}
 *
 * @author  Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version 1
 * @since   19/11/2018.
 */
@RunWith(MockitoJUnitRunner::class)
class TransactionsPresenterTest {

    private val immediate = object : Scheduler() {
        override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
            return super.scheduleDirect(run, 0, unit)
        }

        override fun createWorker(): Scheduler.Worker {
            return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
        }
    }

    @Mock
    private lateinit var view: TransactionsView

    @Mock
    private lateinit var api: OrangeBankApi

    private lateinit var schedulerProvider: RxSchedulersProvider

    private lateinit var transformer: TransactionsTransformer

    private lateinit var repository: TransactionsRepositoryImpl

    private lateinit var useCase: TransactionsUseCase

    private lateinit var presenter: TransactionsPresenter

    @Before
    fun setUp() {
        transformer = TransactionsTransformer()
        repository = TransactionsRepositoryImpl(api, transformer)
        schedulerProvider = object : RxSchedulersProvider {
            override fun ui(): Scheduler {
                return immediate
            }

            override fun io(): Scheduler {
                return immediate
            }
        }
        useCase = TransactionsUseCase(repository, schedulerProvider)

        presenter = TransactionsPresenter(useCase)
        presenter.attachView(view)
    }

    @Test
    fun loadTransactions_whenFirstLoadAndShowLoadingAndSuccess_shouldShowTransactions() {
        val mockResponse = buildTransactionListResponse()

        `when`(api.getTransactions()).thenReturn(Single.just(mockResponse))

        presenter.loadTransactions(true, true)

        assert(presenter.getCurrentTransactions().isNotEmpty())

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).hideTransactionsNotAvailableScreen()
        verify(view).showTransactions(ArgumentMatchers.anyList<Transaction>())
    }

    @Test
    fun loadTransactions_whenFirstLoadAndHideLoadingAndSuccess_shouldShowTransactions() {
        val mockResponse = buildTransactionListResponse()

        `when`(api.getTransactions()).thenReturn(Single.just(mockResponse))

        presenter.loadTransactions(true, true)

        assert(presenter.getCurrentTransactions().isNotEmpty())

        verify(view).hideLoading()
        verify(view).hideTransactionsNotAvailableScreen()
        verify(view).showTransactions(ArgumentMatchers.anyList<Transaction>())
    }

    @Test
    fun loadTransactions_whenFirstLoadAndSuccessWithEmptyTransactionList_shouldShowTransactionsNotAvailableScreen() {
        val mockResponse = emptyList<TransactionOutputResponse>()

        `when`(api.getTransactions()).thenReturn(Single.just(mockResponse))

        presenter.loadTransactions(true, true)

        assert(presenter.getCurrentTransactions().isEmpty())

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).showTransactionsNotAvailableScreen()
    }

    @Test
    fun loadTransactions_whenNotFirstLoadAndSuccessWithEmptyTransactionList_shouldShowTransactions() {
        val mockResponse = buildTransactionListResponse()

        `when`(api.getTransactions()).thenReturn(Single.just(mockResponse))

        presenter.loadTransactions(false, true)

        assert(presenter.getCurrentTransactions().isNotEmpty())

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).hideTransactionsNotAvailableScreen()
        verify(view).showTransactions(ArgumentMatchers.anyList<Transaction>())
    }

    @Test
    fun loadTransactions_whenFirstLoadAndError_shouldShowErrorMessageAndTransactionsNotAvailableScreen() {
        val errorException = Throwable("Error")

        `when`(api.getTransactions()).thenReturn(Single.error(errorException))

        presenter.loadTransactions(true, true)

        assert(presenter.getCurrentTransactions().isEmpty())

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).showTransactionsNotAvailableScreen()
        verify(view).showErrorMessage(errorException.message.orEmpty())
    }

    @Test
    fun loadTransactions_whenNotFirstLoadAndError_shouldShowErrorMessage() {
        val errorException = Throwable("Error")

        `when`(api.getTransactions()).thenReturn(Single.error(errorException))

        presenter.loadTransactions(false, true)

        assert(presenter.getCurrentTransactions().isEmpty())

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).showErrorMessage(errorException.message.orEmpty())
    }

    @Test
    fun loadRestoredTransactions_whenRestoredTransactionsAreNotEmpty_shouldShowRestoreTransactions() {
        val restoredTransactions = buildTransactionList()

        presenter.loadRestoredTransactions(restoredTransactions)

        assert(presenter.getCurrentTransactions().isNotEmpty())

        verify(view).showTransactions(restoredTransactions.takeLast(restoredTransactions.size - 1))
        verify(view).showHeaderTransaction(restoredTransactions.first())
    }

    @Test
    fun loadRestoredTransactions_whenRestoredTransactionsAreEmpty_shouldDoNothing() {
        presenter.loadRestoredTransactions(emptyList())

        assert(presenter.getCurrentTransactions().isEmpty())
    }

}