package com.dmanluc.orangebanktest.utils

import com.dmanluc.orangebanktest.data.contract.TransactionOutputResponse
import com.dmanluc.orangebanktest.domain.entity.Transaction

/**
 * Builder extensions for unit testing
 *
 * @author   Daniel Manrique Lucas <dmanluc91@gmail.com>
 * @version  1
 * @since    18/11/2018
 */

internal fun buildTransactionListResponse() = IntRange(0,10).map { buildTransactionResponse(it) }

private fun buildTransactionResponse(id: Int) = TransactionOutputResponse(id, "2018-07-29T17:56:43.000Z", 100.00, 0.00, "")

internal fun buildTransactionList() = IntRange(0,10).map { buildTransaction(it) }

private fun buildTransaction(id: Int) = Transaction(id, null, 100.00, 0.00, "")

